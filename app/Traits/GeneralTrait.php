<?php

namespace App\Traits;


trait GeneralTrait {

    public function isCodeCorrect($user = null, $code, $type): bool {
        if (!$user || $code != $user->otps()->where('type', $type)->where('status', 'active')->latest()->first()?->otp
          //|| $user->code_expire->isPast()
          //|| env('RESET_CODE') != $code
        ) {
          return false;
        }
        return true;
    }
}
