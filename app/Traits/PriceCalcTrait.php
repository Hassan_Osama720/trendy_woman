<?php

namespace App\Traits;

use App\Models\Order;
use App\Models\SiteSetting;

trait PriceCalcTrait {
    public function price(Order $order) {
        $price = 0;
        if ($order->orderable_type == 'App\Models\Package') {
            $package = $order->orderable;
            $price = $package->price;
        } else {
            $price = SiteSetting::where('key', 'price')->first()->value ?? 0;
        }
        $vatData = $this->vat($price);
        $order->update([
            'price' => $price,
            'vat_per' => $vatData['vat_rate'],
            'vat_amount' => $vatData['vat_amount'],
            'final_total' => $price + $vatData['vat_amount']
        ]);
    }

    public function vat($price): array
    {
        // get vat rate from settings
        $vatRate = SiteSetting::where('key', 'vat_rate')->first()->value ?? 0;
        return [
            'vat_rate' => $vatRate,
            'vat_amount' => ($price * $vatRate) / 100
        ];
    }

}