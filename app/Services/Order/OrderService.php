<?php
namespace App\Services\Order;

use App\Http\Requests\Api\OrderRequest;
use App\Models\Order;
use App\Models\Package;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\DB;

class OrderService {

    public function create(OrderRequest $request)
    {
        $data = array_merge($request->validated(), $this->price($request));
        if (isset($request->package_id)) {
            $data['orderable_id'] = $request->package_id;
            $data['orderable_type'] = Package::class;
        }
        try {
            DB::beginTransaction();
            $order = auth()->user()->orders()->create($data);

            if ($request->hasFile('images')) {
                $images = [];
                foreach ($request->file('images') as $image) {
                    $images[] = ['image' => $image];
                }
                $order->orderImages()->createMany($images);
            }
            DB::commit();
            $order->refresh();
            return $order;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
    public function price(OrderRequest $request): array
    {
        $price = 0;
        if ($request->package_id) {
            $package = Package::find($request->package_id);
            $price = $package->price;
        } else {
            $price = SiteSetting::where('key', 'price')->first()->value ?? 0;
        }
        $vatData = $this->vat($price);
       return [
            'price' => $price,
            'vat_per' => $vatData['vat_rate'],
            'vat_amount' => $vatData['vat_amount'],
            'final_total' => $price + $vatData['vat_amount']
        ];
    }

    public function vat($price): array
    {
        $vatRate = SiteSetting::where('key', 'vat_rate')->first()->value ?? 0;
        return [
            'vat_rate' => $vatRate,
            'vat_amount' => ($price * $vatRate) / 100
        ];
    }

}
