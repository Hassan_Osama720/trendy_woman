<?php

namespace App\Enums;

/**
 * Class OrderStatus
 *
 * @method static string all()
 * @method static string|null nameFor($value)
 * @method static array toArray()
 * @method static array forApi()
 * @method static string slug(int $value)
 */
class ContactUsMessageType extends Base
{
    public const REQUEST = 'request';
    public const COMPLAINT = 'complaint';
    public const SUGGESTION = 'suggestion';
    public const OTHER = 'other';
}
