<?php

namespace App\Enums;

/**
 * Class OrderStatus
 *
 * @method static string all()
 * @method static string|null nameFor($value)
 * @method static array toArray()
 * @method static array forApi()
 * @method static string slug(int $value)
 */
class OrderStatus extends Base
{
    public const NEW      = 'new';
    public const ACCEPTED = 'accepted';
    public const IN_PROGRESS = 'in_progress';
    public const CANCEL   = 'cancel';
    public const FINISHED = 'finished';
}
