<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rate extends BaseModel
{
    protected $fillable = ['order_id', 'rate', 'comment'];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
