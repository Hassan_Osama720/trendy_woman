<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Size extends BaseModel
{
    use HasTranslations;
    protected $fillable = ['name', 'value'];
    public $translatable = ['name'];
}
