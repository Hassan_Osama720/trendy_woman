<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Product extends BaseModel
{
    use HasFactory;
    use HasTranslations;

    const IMAGEPATH = 'products';

    protected $fillable = ['name','price','category_id','image'];
    public $translatable = ['name'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
