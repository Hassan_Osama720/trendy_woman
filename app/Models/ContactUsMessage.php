<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUsMessage extends BaseModel
{
    protected $fillable = [
        'name',
        'phone',
        'country_code',
        'email',
        'message',
        'type',
        'reply',
    ];
}
