<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Store extends BaseModel
{
    use HasTranslations; 

    const IMAGEPATH = 'stores' ; 
    protected $fillable = ['name', 'address', 'phone', 'image'];
    public $translatable = ['name'];
    
}
