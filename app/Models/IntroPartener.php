<?php

namespace App\Models;

class IntroPartener extends BaseModel
{
    protected $fillable = ['name', 'image', 'is_active'];
    const IMAGEPATH = 'parteners' ; 
}
