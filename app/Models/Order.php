<?php

namespace App\Models;

use App\Events\NewOrderCreated;
use App\Events\OrderStatusChanged;
use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends BaseModel {
    use UploadTrait;

    protected $fillable = [
        'user_id',
        'order_num',
        'price',
        'balance',
        'vat_per',
        'vat_amount',
        'final_total',
        'status',
        'name',
        'phone',
        'country_code',
        'age',
        'type',
        'date',
        'cancel_reason',
        'occasion',
        'body_type_id',
        'size_id',
        'orderable_id',
        'orderable_type',
    ];

    public function user() {
    return $this->belongsTo(User::class);
    }

    public function orderImages() {
        return $this->hasMany(OrderImage::class);
    }

    public function orderable() {
        return $this->morphTo();
    }

    public function rate(): HasOne
    {
        return $this->hasOne(Rate::class);
    }

    public function bodyType(): BelongsTo
    {
        return $this->belongsTo(BodyType::class);
    }

    public function size(): BelongsTo
    {
        return $this->belongsTo(Size::class);
    }

    public function getStatusTextAttribute($value) {
        return __('order.status.' . $this->status);
    }

    public static function boot() {
        parent::boot();
        self::creating(function ($model) {
          $lastId = self::max('id') ?? 0;
          $model->order_num = date('Y') . ($lastId + 1);
        });

        self::created(function ($model) {
          event(new NewOrderCreated($model));
        });

        self::updated(function ($model) {
            event(new OrderStatusChanged($model));
        });
    }

}
