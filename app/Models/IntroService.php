<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class IntroService extends BaseModel
{
    const IMAGEPATH = 'services';
    use HasTranslations; 
    protected $fillable = ['title' , 'description','image' , 'is_active'];
    public $translatable = ['title' , 'description'];
}
