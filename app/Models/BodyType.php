<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class BodyType extends BaseModel
{
    use HasTranslations;
    const IMAGEPATH = 'body_types';

    protected $fillable = ['name', 'image'];
    public $translatable = ['name'];
}
