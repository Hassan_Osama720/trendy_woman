<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderImage extends BaseModel
{
    const IMAGEPATH = 'order_images';

    protected $fillable = ['order_id', 'image'];

    public function order() {
        return $this->belongsTo(Order::class);
    }
}
