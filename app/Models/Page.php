<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Page extends BaseModel
{
    use HasTranslations;

    const IMAGEPATH = 'pages';

    protected $fillable = ['title' , 'slug' , 'content', 'image'];
    public $translatable = ['title' , 'content'];
}
