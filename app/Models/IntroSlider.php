<?php

namespace App\Models;


class IntroSlider extends BaseModel
{
    const IMAGEPATH = 'intro_sliders' ; 
    protected $fillable = ['image','is_active','is_static'];
}
