<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class Package extends BaseModel
{
    use HasTranslations;

    protected $fillable = ['name','description','price','is_active'];
    public $translatable = ['name','description'];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
