<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntroSocial extends BaseModel
{
    const IMAGEPATH = 'socials';
    protected $fillable = ['key','url','image'];
}
