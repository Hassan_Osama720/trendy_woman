<?php

namespace App\Listeners;

use App\Enums\OrderStatus;
use App\Events\OrderStatusChanged;
use App\Models\Admin;
use App\Notifications\NotifyAdmin;
use App\Notifications\NotifyUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendStatusOrderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderStatusChanged  $event
     * @return void
     */
    public function handle(OrderStatusChanged $event)
    {
        $notificationData = [
            'title_ar'      => __('notification.title_order_'.$event->order->status, [], 'ar'),
            'title_en'      => __('notification.title_order_'.$event->order->status, [], 'en'),
            'body_ar'       => __('notification.body_order_'.$event->order->status, ['order_num' => $event->order->order_num], 'ar'),
            'body_en'       => __('notification.body_order_'.$event->order->status, ['order_num' => $event->order->order_num], 'en'),
            'order_id'      => $event->order->id,
            'order_num'     => $event->order->order_num,
            'type'          => 'order_'.$event->order->status,
        ];

        if ($event->order->status == OrderStatus::FINISHED || $event->order->status ==OrderStatus::ACCEPTED)
        {
            Notification::send([$event->order->user], new NotifyUser($notificationData));
        } elseif ($event->order->status == OrderStatus::CANCEL || $event->order->status == OrderStatus::IN_PROGRESS)
        {
            $admins = Admin::where('is_notify', 1)->where('is_blocked', 0)->get();
            Notification::send($admins, new NotifyAdmin($notificationData));
        }
    }
}
