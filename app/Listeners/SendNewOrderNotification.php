<?php

namespace App\Listeners;

use App\Events\NewOrderCreated;
use App\Models\Admin;
use App\Notifications\NotifyAdmin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendNewOrderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NewOrderCreated  $event
     * @return void
     */
    public function handle(NewOrderCreated $event)
    {
        $admins = Admin::where('is_blocked', false)->where('is_notify', true)->get();
        $notificationData = [
            'sender'        => auth()->id(),
            'sender_model'  => 'User',
            'title_ar'      => __('notification.new_order_title', [], 'ar'),
            'title_en'      => __('notification.new_order_title', [], 'en'),
            'body_ar'       => __('notification.new_order_body', ['order_num' => $event->order->order_num], 'ar'),
            'body_en'       => __('notification.new_order_body', ['order_num' => $event->order->order_num], 'en'),
            'order_id'      => $event->order->id,
            'type'          => 'new_order',
        ];
        Notification::send($admins, new NotifyAdmin($notificationData));
    }
}
