<?php

namespace App\Http\Requests\Api;

use App\Enums\OrderStatus;
use App\Http\Requests\Api\BaseApiRequest;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderCancelRequest extends BaseApiRequest {

    public function rules() {
        return [
            'order_id' => 'required|exists:orders,id',
            'reason' => 'required|string',
        ];
    }

    public function withValidator($validator) {
        $validator->after(function ($validator) {
            $order = Order::findOrFail($this->order_id);
            if ($order->user_id != auth()->id()) {
                $validator->errors()->add('order_id', __('validation.not_authorized_to_cancel_order'));
            }
            if ($order->status != OrderStatus::NEW) {
                $validator->errors()->add('order_id', __('validation.order_can_not_be_canceled'));
            }
        });
    }
}
