<?php

namespace App\Http\Requests\Api;

use App\Enums\ContactUsMessageType;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class ContactUsMessageRequest extends BaseApiRequest {

    public function rules() {
        return [
            'name'         => 'required|string',
            'type'         => 'required|in:'.ContactUsMessageType::all(),
            'message'      => 'required|string',
        ];
    }
}
