<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class OrderRequest extends BaseApiRequest {

    public function rules() {
        return [
            'name'         => 'required|string',
            'country_code' => 'required|numeric|digits_between:2,5',
            'phone'        => 'required|numeric|digits_between:9,10',
            'age'          => 'required|integer',
            'type'         => 'required|in:male,female',
            'date'         => 'required|date',
            'package_id'   => 'nullable|exists:packages,id,is_active,1',
            'body_type_id' => 'required|exists:body_types,id',
            'size_id'      => 'required|exists:sizes,id',
            'occasion'     => 'required|string',
            'balance'      => 'required|numeric',
            'images'       => 'nullable|array',
            'images.*'     => 'nullable|image',
        ];
    }
}
