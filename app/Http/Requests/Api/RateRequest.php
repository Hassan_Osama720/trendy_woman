<?php

namespace App\Http\Requests\Api;

use App\Enums\OrderStatus;
use App\Http\Requests\Api\BaseApiRequest;
use App\Models\Order;
use Illuminate\Http\Request;

class RateRequest extends BaseApiRequest {

    public function rules() {
        return [
            'order_id'  => 'required|exists:orders,id',
            'rate'      => 'required|numeric|min:1|max:5',
        ];
    }

    public function withValidator($validator) {
        $validator->after(function ($validator) {
            $order = Order::findOrfail($this->order_id);
            if ($order && $order->user_id != auth()->id()) {
                $validator->errors()->add('order_id', __('validation.order_not_found'));
            }
            if ($order && $order->status != OrderStatus::FINISHED) {
                $validator->errors()->add('order_id',  __('validation.order_is_not_finished_yet'));
            }
            if ($order && $order->rate) {
                $validator->errors()->add('order_id',  __('validation.order_is_already_rated'));
            }
        });
    }
}
