<?php

namespace App\Http\Requests\Api\User\Profile;

use App\Models\User;
use App\Traits\GeneralTrait;
use Illuminate\Foundation\Http\FormRequest;

class ChangePhoneCheckCodeRequest extends FormRequest
{
    use GeneralTrait;
    public function rules() {
        return [
            'code'         => 'required',
            'country_code' => 'required|numeric|digits_between:1,5',
            'phone'        => 'required|numeric|digits_between:9,10|unique:users,phone',
        ];
    }

    public function withValidator($validator){
        $validator->after(function ($validator) {

            //$user = User::where(['phone' => $this->phone, 'country_code' => $this->country_code])->first()  ;
            $user = auth()->user();

            if (!$this->isCodeCorrect($user, $this->code, 'change_phone')) {
                $validator->errors()->add('wrong_code', trans('auth.code_invalid'));
            }

        });
    }
}
