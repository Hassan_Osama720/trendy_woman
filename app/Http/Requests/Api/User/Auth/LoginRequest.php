<?php
namespace App\Http\Requests\Api\User\Auth;
use App\Http\Requests\BaseRequest;

class LoginRequest extends BaseRequest {

  public function rules() {
    return [
      'country_code' => 'required|numeric|digits_between:1,5',
        // if phone is numeric validate it to be between 9 and 10 digits and exists in users table if not validate it to be string and exists in users table as name
      'phone'        => 'required|'. (is_numeric($this->phone) ? 'numeric||digits_between:9,10|' : 'string|') .'exists:users,'. (is_numeric($this->phone) ? 'phone' : 'name') .',deleted_at,NULL',
      //'email'       => 'required|email|exists:users,email|max:50', // use on login with email 
      'password'    => 'required|min:6|max:100',
      'device_id'   => 'required|max:250',
      'device_type' => 'required|in:ios,android,web',
      'lang'        => 'required|in:'. implode(',', languages())
    ];
  }

  public function prepareForValidation(){
    $this->merge([
      'phone' => is_numeric($this->phone) ? fixPhone($this->phone) : $this->phone,
      'country_code' => fixPhone($this->country_code),
    ]);
  }

  public function messages() {
    return [
      'phone.exists' => is_numeric($this->phone) ? __('auth.incorrect_key_or_phone') : __('auth.incorrect_name'),
    ];
  }
}
