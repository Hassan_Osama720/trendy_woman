<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Package\ServiceCollection;
use App\Http\Resources\Api\Package\PackageResource;
use App\Models\Package;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    use ResponseTrait;

    public function index(Request $request)
    {
        $packages = Package::where('is_active', true)->paginate($this->paginateNum());
        return $this->successData(PackageResource::make($packages));
    }

    public function show($id)
    {
        $package = Package::findOrFail($id);
        return $this->successData(PackageResource::make($package));
    }
}
