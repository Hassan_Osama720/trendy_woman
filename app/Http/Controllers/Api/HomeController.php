<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Service\ServiceResource;
use App\Http\Resources\Api\Settings\PageResource;
use App\Http\Resources\Api\Settings\PartenerResource;
use App\Http\Resources\Api\Settings\SliderResource;
use App\Http\Resources\Api\Settings\SocialResource;
use App\Models\IntroPartener;
use App\Models\IntroService;
use App\Models\IntroSlider;
use App\Models\Page;
use App\Models\Social;
use App\Traits\ResponseTrait;

class HomeController extends Controller
{
    use ResponseTrait;

    public function all()
    {
        return $this->successData([
            'static_sliders' => SliderResource::collection(
                IntroSlider::where('is_static', 1)->where('is_active',true)->get()
            ),
            'dynamic_sliders' => SliderResource::collection(
                IntroSlider::where('is_static', 0)->where('is_active',true)->get()
            ),
            'services' => ServiceResource::collection(
                IntroService::where('is_active', true)->inRandomOrder()->limit(4)->get()
            ),
            'about_us' => PageResource::make(
                Page::where('slug', 'about')->first()
            ),
            'parteners' => PartenerResource::collection(
                IntroPartener::where('is_active', true)->get()
            ),
            'socials' => SocialResource::collection(
                Social::all()
            )
        ]);
    }

    public function pages($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if (!$page) {
            return $this->failMsg('Page not found');
        }
        return $this->successData(
            PageResource::make(
                $page
            ),
        );
    }

    public function services()
    {
        return $this->successData(
            ServiceResource::collection(
                IntroService::where('is_active', true)->inRandomOrder()->limit(4)->get()
            ),
        );
    }

    public function parteners()
    {
        return $this->successData(
            PartenerResource::collection(
                IntroPartener::where('is_active', true)->get()
            ),
        );
    }

    public function sliders()
    {
        return $this->successData([
            'static_sliders' => SliderResource::collection(
                IntroSlider::where('is_static', 1)->where('is_active',true)->get()
            ),
            'dynamic_sliders' => SliderResource::collection(
                IntroSlider::where('is_static', 0)->where('is_active',true)->get()
            ),
        ]);
    }

    public function socials()
    {
        return $this->successData(
            SocialResource::collection(
                Social::all()
            ),
        );
    }
}
