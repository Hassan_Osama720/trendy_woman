<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Service\ServiceCollection;
use App\Http\Resources\Api\Service\ServiceResource;
use App\Models\IntroService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    use ResponseTrait;

    public function index(Request $request)
    {
        $packages = IntroService::where('is_active', true)->paginate($this->paginateNum());
        return $this->successData(ServiceCollection::make($packages));
    }

    public function show($id)
    {
        $package = IntroService::findOrFail($id);
        return $this->successData(ServiceResource::make($package));
    }
}
