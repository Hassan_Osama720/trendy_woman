<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\SizeResource;
use App\Models\Size;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $sizes = Size::all();
        return $this->successData(SizeResource::collection($sizes));
    }
}
