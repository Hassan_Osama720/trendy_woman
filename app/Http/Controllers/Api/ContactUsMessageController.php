<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ContactUsMessageRequest;
use App\Http\Requests\Api\OrderRequest;
use App\Http\Requests\Api\RateRequest;
use App\Http\Resources\Api\Order\OrderCollection;
use App\Http\Resources\Api\Order\OrderDetailsResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\ContactUsMessage;
use App\Models\Order;
use App\Models\Package;
use App\Services\Order\OrderService;
use App\Traits\PriceCalcTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;


class ContactUsMessageController extends Controller
{
    use ResponseTrait;
    public function store(ContactUsMessageRequest $request): \Illuminate\Http\JsonResponse
    {
        $message = ContactUsMessage::create($request->validated());
        return $this->successMsg('Message sent successfully');
    }

}
