<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\BodyTypeResource;
use App\Models\BodyType;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BodyTypeController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $bodyTypes = BodyType::all();
        return $this->successData(BodyTypeResource::collection($bodyTypes));
    }
}
