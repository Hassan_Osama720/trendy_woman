<?php

namespace App\Http\Controllers\Api;

use App\Enums\OrderStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderCancelRequest;
use App\Http\Requests\Api\OrderRequest;
use App\Http\Requests\Api\RateRequest;
use App\Http\Resources\Api\Order\OrderCollection;
use App\Http\Resources\Api\Order\OrderDetailsResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Admin;
use App\Models\Order;
use App\Models\SiteSetting;
use App\Notifications\NotifyAdmin;
use App\Services\Order\OrderService;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Notification;


class OrderController extends Controller
{
    use ResponseTrait;

    public $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    private function getOrders($status): \Illuminate\Pagination\LengthAwarePaginator
    {
        return auth()->user()->orders()->whereIn('status', $status)->search(request()->all())->with('orderable')->latest()->paginate($this->paginateNum());
    }
    public function newOrders(): \Illuminate\Http\JsonResponse
    {
        $orders = $this->getOrders(['new']);
        return $this->successData(OrderCollection::make($orders));
    }

    public function inProgressOrders(): \Illuminate\Http\JsonResponse
    {
        $orders = $this->getOrders(['in_progress']);
        return $this->successData(OrderCollection::make($orders));
    }

    public function completedOrders(): \Illuminate\Http\JsonResponse
    {
        $orders = $this->getOrders(['finished', 'cancel']);
        return $this->successData(OrderCollection::make($orders));
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        $order = Order::with(['orderable','orderImages','rate','bodyType','size'])
            ->findOrfail($id);
        if ($order->user_id != auth()->id()) {
            return $this->failMsg(__('validation.not_authorized_to_show_order'));
        }
        return $this->successData(new OrderDetailsResource($order));
    }

    public function store(OrderRequest $request): \Illuminate\Http\JsonResponse
    {
        $order = $this->orderService->create($request);
        if (!is_string($order)) {
            return $this->successData(new OrderResource($order));
        }
        return $this->failMsg($order);
    }

    public function addRate(RateRequest $request): \Illuminate\Http\JsonResponse
    {
        $order = Order::findOrFail($request->order_id);
        $order->rate()->create([
            'rate' => $request->get('rate'),
        ]);
        return $this->successMsg(__('apis.rate_added'));
    }

    public function payOrder($id): \Illuminate\Http\JsonResponse
    {
        $order = Order::findOrFail($id);
        if ($order->user_id != auth()->id()) {
            return $this->failMsg(__('validation.not_authorized_to_pay_order'));
        }
        if ($order->status != OrderStatus::ACCEPTED) {
            return $this->failMsg(__('validation.order_not_accepted'));
        }

        $order->update(['status' => OrderStatus::IN_PROGRESS]);
        return $this->successMsg(__('apis.order_paid'));
    }

    public function cancelOrder(OrderCancelRequest $request): \Illuminate\Http\JsonResponse
    {
        $order = Order::findOrFail($request->order_id);
        $order->update(['status' => OrderStatus::CANCEL , 'cancel_reason' => request('cancel_reason')]);
        return $this->successMsg(__('apis.order_canceled'));
    }

    public function getPrice(): \Illuminate\Http\JsonResponse
    {
        $price = SiteSetting::where('key', 'vat_rate')->first()->value ?? 0;
        return $this->successData(['price' => $price]);
    }

}
