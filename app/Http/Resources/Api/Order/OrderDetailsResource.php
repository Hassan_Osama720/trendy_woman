<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\Package\PackageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'order_num'     => $this->order_num,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'country_code'  => $this->country_code,
            'age'           => $this->age,
            'type'          => $this->type,
            'date'          => $this->date,
            'package'       => $this->when($this->orderable , new PackageResource($this->orderable)),
            'body_type'     => $this->when($this->bodyType, $this->bodyType->name),
            'size'          => $this->when($this->size, $this->size->name),
            'price'         => $this->price,
            'balance'       => $this->balance,
            'vat_amount'    => $this->vat_amount,
            'final_total'   => $this->final_total,
            'status'        => $this->status,
            'status_text'   => $this->status_text,
            'occasion'      => $this->occasion,
            'rate'          => $this->when($this->rate, $this->rate?->rate),
            'created_at'    => $this->created_at->format('Y-m-d H:i:s'),
            'images'        => $this->orderImages->pluck('image'),
        ];
    }
}
