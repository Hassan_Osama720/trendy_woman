<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\Package\PackageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'order_num'     => $this->order_num,
            'price'         => $this->price,
            'vat_amount'    => $this->vat_amount,
            'final_total'   => $this->final_total,
            'status'        => $this->status,
            'status_text'   => $this->status_text,
            'created_at'    => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
