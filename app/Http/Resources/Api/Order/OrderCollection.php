<?php

namespace App\Http\Resources\Api\Order;

use App\Traits\PaginationTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    use PaginationTrait;

    public function toArray($request)
    {
        return [
            'data'       => OrderResource::collection($this->collection),
            'pagination' => $this->paginationModel($this),
        ];

    }
}
