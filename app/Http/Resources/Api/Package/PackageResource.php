<?php

namespace App\Http\Resources\Api\Package;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource {

  public function toArray($request) {
      return [
        'id'                    => $this->id,
        'name'                  => $this->name,
        'description'           => $this->description,
        'price'                 => $this->price,
      ];
  }
}
