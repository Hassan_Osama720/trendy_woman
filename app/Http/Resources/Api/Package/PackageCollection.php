<?php

namespace App\Http\Resources\Api\Package;

use App\Traits\PaginationTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageCollection extends ResourceCollection
{
    use PaginationTrait;

    public function toArray($request)
    {
        return [
            'data'       => PackageResource::collection($this->collection),
            'pagination' => $this->paginationModel($this),
        ];

    }
}
