<?php

namespace App\Http\Resources\Api\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource {

  public function toArray($request) {
    if($request->route()->getName() == 'products.show') {
      return [
        'id'                    => $this->id,
        'name'                  => $this->name,
        'price'                 => $this->price,
        'category'              => $this->category->name,
        'image'                 => $this->image,
        'created_at'            => $this->created_at->format('Y-m-d H:i'),
        'updated_at'            => $this->updated_at->format('Y-m-d H:i'),
      ];
    }
    return [
      'id'                    => $this->id,
      'name'                  => $this->name,
      'price'                 => $this->price,
      'category'              => $this->category->name,
    ];
  }
}
