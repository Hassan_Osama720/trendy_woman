<?php

namespace App\Http\Resources\Api\Service;

use App\Traits\PaginationTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceCollection extends ResourceCollection
{
    use PaginationTrait;

    public function toArray($request)
    {
        return [
            'data'       => ServiceResource::collection($this->collection),
            'pagination' => $this->paginationModel($this),
        ];

    }
}
