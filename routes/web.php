<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'admin.'], function () {

  // Set Language For Admin 
  Route::get('/lang/{lang}', 'AuthController@SetLanguage');

  // guest routes for admin 
  Route::group(['middleware' => ['guest:admin']], function () {
    Route::get('login', 'AuthController@showLoginForm')->name('show.login')->middleware('guest:admin');
    Route::post('login', 'AuthController@login')->name('login');
  });


  
  Route::get('logout', 'AuthController@logout')->name('logout');
  
  Route::group(['middleware' => ['admin']], function () {

    /*------------ start Of profile----------*/
      Route::get('profile', [
        'uses'  => 'HomeController@profile',
        'as'    => 'profile.profile',
        'title' => 'profile',
      ]);

      Route::put('profile-update', [
        'uses'  => 'HomeController@updateProfile',
        'as'    => 'profile.update',
        'title' => 'update_profile',
      ]);
      Route::put('profile-update-password', [
        'uses'  => 'HomeController@updatePassword',
        'as'    => 'profile.update_password',
        'title' => 'update_password',
      ]);
    /*------------ end Of profile----------*/

    /*------------ start Of Dashboard----------*/
      Route::get('dashboard', [
        'uses'      => 'HomeController@dashboard',
        'as'        => 'dashboard',
        'icon'      => '<i class="feather icon-home"></i>',
        'title'     => 'main_page',
        'has_sub_route' => false,
        'type'      => 'parent',
      ]);
    /*------------ end Of dashboard ----------*/

    /*------------ start Of intro site  ----------*/
      Route::get('intro-site', [
        'icon'      => '<i class="feather icon-map"></i>',
        'title'     => 'introductory_site',
        'type'      => 'parent',
        'has_sub_route' => true,
        'child'     => [
          'intro_settings.index', 'introsliders.show', 'introsliders.index', 'introsliders.store', 'introsliders.update', 'introsliders.delete', 'introsliders.deleteAll', 'introsliders.create', 'introsliders.edit',
          'introservices.show', 'introservices.index', 'introservices.create', 'introservices.store', 'introservices.edit', 'introservices.update', 'introservices.delete', 'introservices.deleteAll',
          'introfqscategories.show', 'introfqscategories.index', 'introfqscategories.store', 'introfqscategories.create', 'introfqscategories.edit', 'introfqscategories.update', 'introfqscategories.delete', 'introfqscategories.deleteAll',
          'introfqs.show', 'introfqs.index', 'introfqs.store', 'introfqs.update', 'introfqs.delete', 'introfqs.deleteAll', 'introfqs.edit', 'introfqs.create',
          'introparteners.create', 'introparteners.show', 'introparteners.index', 'introparteners.store', 'introparteners.update', 'introparteners.delete', 'introparteners.deleteAll',
          'intromessages.index', 'intromessages.delete', 'intromessages.deleteAll', 'intromessages.show',
          'introsocials.show', 'introsocials.index', 'introsocials.store', 'introsocials.update', 'introsocials.delete', 'introsocials.deleteAll', 'introsocials.edit', 'introsocials.create',
          'introparteners.edit', 'introhowworks.show', 'introhowworks.index', 'introhowworks.store', 'introhowworks.update', 'introhowworks.delete', 'introhowworks.deleteAll', 'introhowworks.create', 'introhowworks.edit',
        ],
      ]);

      Route::get('intro-settings', [
        'uses'      => 'IntroSetting@index',
        'as'        => 'intro_settings.index',
        'title'     => 'introductory_site_setting',
        'sub_link'  => true ,
      ]);

      /*------------ start Of introsliders ----------*/
          Route::get('introsliders', [
            'uses'      => 'IntroSliderController@index',
            'as'        => 'introsliders.index',
            'title'     => 'intro_slider.index',
            'sub_link'  => true ,
          ]);

          # socials store
          Route::get('introsliders/create', [
            'uses'  => 'IntroSliderController@create',
            'as'    => 'introsliders.create',
            'title' => 'intro_slider.create_page',
          ]);

          # introsliders store
          Route::post('introsliders/store', [
            'uses'  => 'IntroSliderController@store',
            'as'    => 'introsliders.store',
            'title' => 'intro_slider.create',
          ]);

          # socials update
          Route::get('introsliders/{id}/edit', [
            'uses'  => 'IntroSliderController@edit',
            'as'    => 'introsliders.edit',
            'title' => 'intro_slider.edit_page',
          ]);

          # introsliders update
          Route::put('introsliders/{id}', [
            'uses'  => 'IntroSliderController@update',
            'as'    => 'introsliders.update',
            'title' => 'intro_slider.edit',
          ]);

          # introsliders update
          Route::get('introsliders/{id}/Show', [
            'uses'  => 'IntroSliderController@show',
            'as'    => 'introsliders.show',
            'title' => 'intro_slider.show',
          ]);

          # introsliders delete
          Route::delete('introsliders/{id}', [
            'uses'  => 'IntroSliderController@destroy',
            'as'    => 'introsliders.delete',
            'title' => 'intro_slider.delete',
          ]);

          #delete all introsliders
          Route::post('delete-all-introsliders', [
            'uses'  => 'IntroSliderController@destroyAll',
            'as'    => 'introsliders.deleteAll',
            'title' => 'intro_slider.delete_all',
          ]);
      /*------------ end Of introsliders ----------*/

      /*------------ start Of introservices ----------*/
        Route::get('introservices', [
          'uses'  => 'IntroServiceController@index',
          'as'    => 'introservices.index',
          'title' => 'our_services.index',
          'sub_link'  => true ,
        ]);
        # introservices update
        Route::get('introservices/{id}/Show', [
          'uses'  => 'IntroServiceController@show',
          'as'    => 'introservices.show',
          'title' => 'our_services.show',
        ]);
        # socials store
        Route::get('introservices/create', [
          'uses'  => 'IntroServiceController@create',
          'as'    => 'introservices.create',
          'title' => 'our_services.create_page',
        ]);
        # introservices store
        Route::post('introservices/store', [
          'uses'  => 'IntroServiceController@store',
          'as'    => 'introservices.store',
          'title' => 'our_services.create',
        ]);

        # socials update
        Route::get('introservices/{id}/edit', [
          'uses'  => 'IntroServiceController@edit',
          'as'    => 'introservices.edit',
          'title' => 'our_services.edit_page',
        ]);

        # introservices update
        Route::put('introservices/{id}', [
          'uses'  => 'IntroServiceController@update',
          'as'    => 'introservices.update',
          'title' => 'our_services.edit',
        ]);

        # introservices delete
        Route::delete('introservices/{id}', [
          'uses'  => 'IntroServiceController@destroy',
          'as'    => 'introservices.delete',
          'title' => 'our_services.delete',
        ]);

        #delete all introservices
        Route::post('delete-all-introservices', [
          'uses'  => 'IntroServiceController@destroyAll',
          'as'    => 'introservices.deleteAll',
          'title' => 'our_services.delete_all',
        ]);
      /*------------ end Of introservices ----------*/

      /*------------ start Of introfqscategories ----------*/
        Route::get('introfqscategories', [
          'uses'  => 'IntroFqsCategoryController@index',
          'as'    => 'introfqscategories.index',
          'title' => 'common_questions_sections.index',
          'sub_link'  => true ,
        ]);
        # socials store
        Route::get('introfqscategories/create', [
          'uses'  => 'IntroFqsCategoryController@create',
          'as'    => 'introfqscategories.create',
          'title' => 'common_questions_sections.create_page',
        ]);
        # introfqscategories store
        Route::post('introfqscategories/store', [
          'uses'  => 'IntroFqsCategoryController@store',
          'as'    => 'introfqscategories.store',
          'title' => 'common_questions_sections.create',
        ]);
        # introfqscategories update
        Route::get('introfqscategories/{id}/edit', [
          'uses'  => 'IntroFqsCategoryController@edit',
          'as'    => 'introfqscategories.edit',
          'title' => 'common_questions_sections.edit_page',
        ]);
        # introfqscategories update
        Route::put('introfqscategories/{id}', [
          'uses'  => 'IntroFqsCategoryController@update',
          'as'    => 'introfqscategories.update',
          'title' => 'common_questions_sections.edit',
        ]);

        # introfqscategories update
        Route::get('introfqscategories/{id}/Show', [
          'uses'  => 'IntroFqsCategoryController@show',
          'as'    => 'introfqscategories.show',
          'title' => 'common_questions_sections.show',
        ]);

        # introfqscategories delete
        Route::delete('introfqscategories/{id}', [
          'uses'  => 'IntroFqsCategoryController@destroy',
          'as'    => 'introfqscategories.delete',
          'title' => 'common_questions_sections.delete',
        ]);

        #delete all introfqscategories
        Route::post('delete-all-introfqscategories', [
          'uses'  => 'IntroFqsCategoryController@destroyAll',
          'as'    => 'introfqscategories.deleteAll',
          'title' => 'common_questions_sections.delete_all',
        ]);
      /*------------ end Of introfqscategories ----------*/

      /*------------ start Of introfqs ----------*/
        Route::get('introfqs', [
          'uses'  => 'IntroFqsController@index',
          'as'    => 'introfqs.index',
          'title' => 'questions_sections.index',
          'sub_link'  => true ,
        ]);

        # socials store
        Route::get('introfqs/create', [
          'uses'  => 'IntroFqsController@create',
          'as'    => 'introfqs.create',
          'title' => 'questions_sections.create_page',
        ]);

        # introfqs store
        Route::post('introfqs/store', [
          'uses'  => 'IntroFqsController@store',
          'as'    => 'introfqs.store',
          'title' => 'questions_sections.create',
        ]);
        # introfqscategories update
        Route::get('introfqs/{id}/edit', [
          'uses'  => 'IntroFqsController@edit',
          'as'    => 'introfqs.edit',
          'title' => 'questions_sections.edit_page',
        ]);
        # introfqscategories update
        Route::get('introfqs/{id}/Show', [
          'uses'  => 'IntroFqsController@show',
          'as'    => 'introfqs.show',
          'title' => 'questions_sections.show',
        ]);

        # introfqs update
        Route::put('introfqs/{id}', [
          'uses'  => 'IntroFqsController@update',
          'as'    => 'introfqs.update',
          'title' => 'questions_sections.edit',
        ]);

        # introfqs delete
        Route::delete('introfqs/{id}', [
          'uses'  => 'IntroFqsController@destroy',
          'as'    => 'introfqs.delete',
          'title' => 'questions_sections.delete',
        ]);

        #delete all introfqs
        Route::post('delete-all-introfqs', [
          'uses'  => 'IntroFqsController@destroyAll',
          'as'    => 'introfqs.deleteAll',
          'title' => 'questions_sections.delete_all',
        ]);
      /*------------ end Of introfqs ----------*/

      /*------------ start Of introparteners ----------*/
        Route::get('introparteners', [
          'uses'  => 'IntroPartenerController@index',
          'as'    => 'introparteners.index',
          'title' => 'success_Partners.index',
          'sub_link'  => true ,
        ]);

        # introparteners update
        Route::get('introparteners/{id}/Show', [
          'uses'  => 'IntroPartenerController@show',
          'as'    => 'introparteners.show',
          'title' => 'success_Partners.show',
        ]);

        # socials store
        Route::get('introparteners/create', [
          'uses'  => 'IntroPartenerController@create',
          'as'    => 'introparteners.create',
          'title' => 'success_Partners.create_page',
        ]);

        # introparteners store
        Route::post('introparteners/store', [
          'uses'  => 'IntroPartenerController@store',
          'as'    => 'introparteners.store',
          'title' => 'success_Partners.create',
        ]);

        # introparteners update
        Route::get('introparteners/{id}/edit', [
          'uses'  => 'IntroPartenerController@edit',
          'as'    => 'introparteners.edit',
          'title' => 'success_Partners.edit_page',
        ]);

        # introparteners update
        Route::put('introparteners/{id}', [
          'uses'  => 'IntroPartenerController@update',
          'as'    => 'introparteners.update',
          'title' => 'success_Partners.edit',
        ]);

        # introparteners delete
        Route::delete('introparteners/{id}', [
          'uses'  => 'IntroPartenerController@destroy',
          'as'    => 'introparteners.delete',
          'title' => 'success_Partners.delete',
        ]);

        #delete all introparteners
        Route::post('delete-all-introparteners', [
          'uses'  => 'IntroPartenerController@destroyAll',
          'as'    => 'introparteners.deleteAll',
          'title' => 'success_Partners.delete_all',
        ]);
      /*------------ end Of introparteners ----------*/

      /*------------ start Of intromessages ----------*/
        Route::get('intromessages', [
          'uses'  => 'IntroMessagesController@index',
          'as'    => 'intromessages.index',
          'title' => 'customer_messages.index',
          'sub_link'  => true ,
        ]);

        # socials update
        Route::get('intromessages/{id}', [
          'uses'  => 'IntroMessagesController@show',
          'as'    => 'intromessages.show',
          'title' => 'customer_messages.show',
        ]);

        # intromessages delete
        Route::delete('intromessages/{id}', [
          'uses'  => 'IntroMessagesController@destroy',
          'as'    => 'intromessages.delete',
          'title' => 'customer_messages.delete',
        ]);

        #delete all intromessages
        Route::post('delete-all-intromessages', [
          'uses'  => 'IntroMessagesController@destroyAll',
          'as'    => 'intromessages.deleteAll',
          'title' => 'customer_messages.delete_all',
        ]);
      /*------------ end Of intromessages ----------*/

      /*------------ start Of introsocials ----------*/
        Route::get('introsocials', [
          'uses'  => 'IntroSocialController@index',
          'as'    => 'introsocials.index',
          'title' => 'socials.index',
          'sub_link'  => true ,
        ]);

        # introsocials update
        Route::get('introsocials/{id}/Show', [
          'uses'  => 'IntroSocialController@show',
          'as'    => 'introsocials.show',
          'title' => 'socials.show',
        ]);
        # introsocials store
        Route::get('introsocials/create', [
          'uses'  => 'IntroSocialController@create',
          'as'    => 'introsocials.create',
          'title' => 'socials.create_page',
        ]);

        # introsocials store
        Route::post('introsocials/store', [
          'uses'  => 'IntroSocialController@store',
          'as'    => 'introsocials.store',
          'title' => 'socials.create',
        ]);
        # introsocials update
        Route::get('introsocials/{id}/edit', [
          'uses'  => 'IntroSocialController@edit',
          'as'    => 'introsocials.edit',
          'title' => 'socials.edit_page',
        ]);

        # introsocials update
        Route::put('introsocials/{id}', [
          'uses'  => 'IntroSocialController@update',
          'as'    => 'introsocials.update',
          'title' => 'socials.edit',
        ]);

        # introsocials delete
        Route::delete('introsocials/{id}', [
          'uses'  => 'IntroSocialController@destroy',
          'as'    => 'introsocials.delete',
          'title' => 'socials.delete',
        ]);

        #delete all introsocials
        Route::post('delete-all-introsocials', [
          'uses'  => 'IntroSocialController@destroyAll',
          'as'    => 'introsocials.deleteAll',
          'title' => 'socials.delete_all',
        ]);
      /*------------ end Of introsocials ----------*/

      /*------------ start Of introhowworks ----------*/
        Route::get('introhowworks', [
          'uses'  => 'IntroHowWorkController@index',
          'as'    => 'introhowworks.index',
          'title' => 'how_the_site_works.index',
          'sub_link'  => true ,
        ]);

        # introhowworks store
        Route::get('introhowworks/create', [
          'uses'  => 'IntroHowWorkController@create',
          'as'    => 'introhowworks.create',
          'title' => 'how_the_site_works.create_page',
        ]);
        # introfqscategories update
        Route::get('introhowworks/{id}/Show', [
          'uses'  => 'IntroHowWorkController@show',
          'as'    => 'introhowworks.show',
          'title' => 'how_the_site_works.show',
        ]);

        # introhowworks update
        Route::get('introhowworks/{id}/edit', [
          'uses'  => 'IntroHowWorkController@edit',
          'as'    => 'introhowworks.edit',
          'title' => 'how_the_site_works.edit_page',
        ]);

        # introhowworks store
        Route::post('introhowworks/store', [
          'uses'  => 'IntroHowWorkController@store',
          'as'    => 'introhowworks.store',
          'title' => 'how_the_site_works.create',
        ]);

        # introhowworks update
        Route::put('introhowworks/{id}', [
          'uses'  => 'IntroHowWorkController@update',
          'as'    => 'introhowworks.update',
          'title' => 'how_the_site_works.edit',
        ]);

        # introhowworks delete
        Route::delete('introhowworks/{id}', [
          'uses'  => 'IntroHowWorkController@destroy',
          'as'    => 'introhowworks.delete',
          'title' => 'how_the_site_works.delete',
        ]);

        #delete all introhowworks
        Route::post('delete-all-introhowworks', [
          'uses'  => 'IntroHowWorkController@destroyAll',
          'as'    => 'introhowworks.deleteAll',
          'title' => 'how_the_site_works.delete_all',
        ]);
      /*------------ end Of introhowworks ----------*/

    /*------------ end Of intro site ----------*/

    /*------------ start Of all users  ----------*/
      Route::get('all-users', [
        'as'        => 'all_users',
        'icon'      => '<i class="feather icon-users"></i>',
        'title'     => 'all_users',
        'type'      => 'parent',
        'has_sub_route' => true,
        'child'     => [
          'clients.index', 'clients.show',  'clients.complaints.show', 'clients.store', 'clients.update', 'clients.delete', 'clients.notify', 'clients.deleteAll', 'clients.create', 'clients.edit', 'clients.block','clients.updateBalance',
          'admins.index', 'admins.store', 'admins.update', 'admins.edit', 'admins.delete', 'admins.deleteAll', 'admins.create', 'admins.edit', 'admins.notifications', 'admins.notifications.delete', 'admins.show',
        ],
      ]);

      /*------------ start Of users Controller ----------*/

        Route::get('clients', [
          'uses'  => 'ClientController@index',
          'as'        => 'clients.index',
          'sub_link'  => true ,
          'title'     => 'users.index',
        ]);

        # clients store
        Route::get( 'clients/create',[
            'uses'  => 'ClientController@create',
            'as'    => 'clients.create', 'clients.edit',
            'title' => 'users.create_page',
          ]
        );

        # clients update
        Route::get('clients/{id}/edit', [
          'uses'  => 'ClientController@edit',
          'as'    => 'clients.edit',
          'title' => 'users.edit_page',
        ]);
        #store
        Route::post('clients/store', [
          'uses'  => 'ClientController@store',
          'as'    => 'clients.store',
          'title' => 'users.create',
        ]);

        #update
        Route::put('clients/{id}', [
          'uses'  => 'ClientController@update',
          'as'    => 'clients.update',
          'title' => 'users.edit',
        ]);

        Route::get('clients/{id}/show', [
          'uses'  => 'ClientController@show',
          'as'    => 'clients.show',
          'title' => 'users.show',
        ]);
        Route::get('clients-complaints/{id}/show', [
          'uses'  => 'ClientController@complaints',
          'as'    => 'clients.complaints.show',
          'title' => 'users.complaints',
        ]);

        #delete
        Route::delete('clients/{id}', [
          'uses'  => 'ClientController@destroy',
          'as'    => 'clients.delete',
          'title' => 'users.delete',
        ]);

        #delete
        Route::post('delete-all-clients', [
          'uses'  => 'ClientController@destroyAll',
          'as'    => 'clients.deleteAll',
          'title' => 'users.delete_all',
        ]);

        #notify
        Route::post(
          'admins/clients/notify',
          [
            'uses'  => 'ClientController@notify',
            'as'    => 'clients.notify',
            'title' => 'users.Send_user_notification',
          ]
        );
        Route::post('admins/clients/update-balance/{id}',
          [
            'uses'  => 'ClientController@updateBalance',
            'as'    => 'clients.updateBalance',
            'title' => 'users.updateBalance',
          ]
        );
        Route::post('admins/clients/block-user',[
            'uses'  => 'ClientController@block',
            'as'    => 'clients.block',
            'title' => 'users.block_user',
          ]
        );
      /*------------ end Of users Controller ----------*/

      /*------------ start Of Admins Controller ----------*/
        Route::get('admins', [
          'uses'  => 'AdminController@index',
          'as'    => 'admins.index',
          'title' => 'admins.index',
          'sub_link'  => true ,
        ]);

        # admins store
        Route::get('show-notifications', [
          'uses'  => 'AdminController@notifications',
          'as'    => 'admins.notifications',
          'title' => 'admins.notification_page',
        ]);

        # admins store
        Route::post('delete-notifications', [
          'uses'  => 'AdminController@deleteNotifications',
          'as'    => 'admins.notifications.delete',
          'title' => 'admins.delete_notification',
        ]);

        # admins store
        Route::get(
          'admins/create',
          [
            'uses'  => 'AdminController@create',
            'as'    => 'admins.create',
            'title' => 'admins.create_page',
          ]
        );

        #store
        Route::post('admins/store', [
          'uses'  => 'AdminController@store',
          'as'    => 'admins.store',
          'title' => 'admins.create',
        ]);

        # admins update
        Route::get('admins/{id}/edit', [
          'uses'  => 'AdminController@edit',
          'as'    => 'admins.edit',
          'title' => 'admins.edit_page',
        ]);
        #update
        Route::put(
          'admins/{id}',
          [
            'uses'  => 'AdminController@update',
            'as'    => 'admins.update',
            'title' => 'admins.edit',
          ]
        );

        Route::get('admins/{id}/show', [
          'uses'  => 'AdminController@show',
          'as'    => 'admins.show',
          'title' => 'admins.show',
        ]);

        #delete
        Route::delete('admins/{id}', [
          'uses'  => 'AdminController@destroy',
          'as'    => 'admins.delete',
          'title' => 'admins.delete',
        ]);

        #delete
        Route::post('delete-all-admins', [
          'uses'  => 'AdminController@destroyAll',
          'as'    => 'admins.deleteAll',
          'title' => 'admins.delete_all',
        ]);

      /*------------ end Of admins Controller ----------*/


    /*------------ start Of all users  ----------*/

    /*------------ start Of countries && cities ----------*/
      Route::get('countries-cities', [
        'as'        => 'countries-cities',
        'icon'      => '<i class="feather icon-flag"></i>',
        'title'     => 'countries_cities',
        'type'      => 'parent',
        'has_sub_route' => true,
        'child'     => [
          'countries.index', 'countries.show', 'countries.create', 'countries.store', 'countries.edit', 'countries.update', 'countries.delete', 'countries.deleteAll',
          'regions.index', 'regions.create', 'regions.store', 'regions.edit', 'regions.show', 'regions.update', 'regions.delete', 'regions.deleteAll',
          'cities.index', 'cities.create', 'cities.store', 'cities.edit', 'cities.show', 'cities.update', 'cities.delete', 'cities.deleteAll' ,
        ],
      ]);

      /*------------ start Of countries ----------*/
        Route::get('countries', [
          'uses'      => 'CountryController@index',
          'as'        => 'countries.index',
          'title'     => 'countries.index',
          'sub_link'  => true ,
        ]);
  
        Route::get('countries/{id}/show', [
          'uses'  => 'CountryController@show',
          'as'    => 'countries.show',
          'title' => 'countries.show',
        ]);
  
        # countries store
        Route::get('countries/create', [
          'uses'  => 'CountryController@create',
          'as'    => 'countries.create',
          'title' => 'countries.create_page',
        ]);
  
        # countries store
        Route::post('countries/store', [
          'uses'  => 'CountryController@store',
          'as'    => 'countries.store',
          'title' => 'countries.create',
        ]);
  
        # countries update
        Route::get('countries/{id}/edit', [
          'uses'  => 'CountryController@edit',
          'as'    => 'countries.edit',
          'title' => 'countries.edit_page',
        ]);
  
        # countries update
        Route::put('countries/{id}', [
          'uses'  => 'CountryController@update',
          'as'    => 'countries.update',
          'title' => 'countries.edit',
        ]);
  
        # countries delete
        Route::delete('countries/{id}', [
          'uses'  => 'CountryController@destroy',
          'as'    => 'countries.delete',
          'title' => 'countries.delete',
        ]);
        #delete all countries
        Route::post('delete-all-countries', [
          'uses'  => 'CountryController@destroyAll',
          'as'    => 'countries.deleteAll',
          'title' => 'countries.delete_all',
        ]);
      /*------------ end Of countries ----------*/
  
      /*------------ start Of cities ----------*/
        Route::get('cities', [
          'uses'      => 'CityController@index',
          'as'        => 'cities.index',
          'title'     => 'cities.index',
          'sub_link'  => true ,
        ]);
  
        # cities store
        Route::get('cities/create', [
          'uses'  => 'CityController@create',
          'as'    => 'cities.create',
          'title' => 'cities.create_page',
        ]);
  
        # cities store
        Route::post('cities/store', [
          'uses'  => 'CityController@store',
          'as'    => 'cities.store',
          'title' => 'cities.create',
        ]);
  
        # cities update
        Route::get('cities/{id}/edit', [
          'uses'  => 'CityController@edit',
          'as'    => 'cities.edit',
          'title' => 'cities.edit_page',
        ]);
  
        # cities update
        Route::put('cities/{id}', [
          'uses'  => 'CityController@update',
          'as'    => 'cities.update',
          'title' => 'cities.edit',
        ]);
  
        Route::get('cities/{id}/show', [
          'uses'  => 'CityController@show',
          'as'    => 'cities.show',
          'title' => 'cities.show',
        ]);
  
        # cities delete
        Route::delete('cities/{id}', [
          'uses'  => 'CityController@destroy',
          'as'    => 'cities.delete',
          'title' => 'cities.delete',
        ]);
        #delete all cities
        Route::post('delete-all-cities', [
          'uses'  => 'CityController@destroyAll',
          'as'    => 'cities.deleteAll',
          'title' => 'cities.delete_all',
        ]);
      /*------------ end Of cities ----------*/

      /*------------ start Of regions ----------*/
        Route::get('regions', [
          'uses'      => 'RegionController@index',
          'as'        => 'regions.index',
          'title'     => 'regions.index',
          'sub_link'  => true ,
        ]);
  
        # regions store
        Route::get('regions/create', [
          'uses'  => 'RegionController@create',
          'as'    => 'regions.create',
          'title' => 'regions.create_page',
        ]);
  
        # regions store
        Route::post('regions/store', [
          'uses'  => 'RegionController@store',
          'as'    => 'regions.store',
          'title' => 'regions.create',
        ]);
  
        # regions update
        Route::get('regions/{id}/edit', [
          'uses'  => 'RegionController@edit',
          'as'    => 'regions.edit',
          'title' => 'regions.edit_page',
        ]);
  
        # regions update
        Route::put('regions/{id}', [
          'uses'  => 'RegionController@update',
          'as'    => 'regions.update',
          'title' => 'regions.edit',
        ]);
  
        Route::get('regions/{id}/show', [
          'uses'  => 'RegionController@show',
          'as'    => 'regions.show',
          'title' => 'regions.show',
        ]);
  
        # regions delete
        Route::delete('regions/{id}', [
          'uses'  => 'RegionController@destroy',
          'as'    => 'regions.delete',
          'title' => 'regions.delete',
        ]);
        #delete all regions
        Route::post('delete-all-regions', [
          'uses'  => 'RegionController@destroyAll',
          'as'    => 'regions.deleteAll',
          'title' => 'regions.delete_all',
        ]);
      /*------------ end Of regions ----------*/
    /*------------ end Of countries && cities ----------*/

    /*------------ start Of public settings ----------*/
        Route::get('public-settings', [
            'as'        => 'public-settings',
            'icon'      => '<i class="feather icon-flag"></i>',
            'title'     => 'public_settings',
            'type'      => 'parent',
            'has_sub_route' => true,
            'child'     => [
                'socials.index', 'socials.show', 'socials.create', 'socials.store', 'socials.show', 'socials.update', 'socials.edit', 'socials.delete', 'socials.deleteAll',
                'seos.show', 'seos.create', 'seos.edit',  'seos.index', 'seos.store', 'seos.update', 'seos.delete', 'seos.deleteAll',
                'roles.index', 'roles.create', 'roles.store', 'roles.edit', 'roles.update', 'roles.delete',
                'settings.index', 'settings.update',
                'reports', 'reports.delete', 'reports.deleteAll', 'reports.show'
            ],
        ]);

      /*------------ start Of socials ----------*/
        Route::get('socials', [
          'uses'      => 'SocialController@index',
          'as'        => 'socials.index',
          'title'     => 'socials.index',
          'sub_link'  => true ,
        ]);
        # socials update
        Route::get('socials/{id}/Show', [
          'uses'  => 'SocialController@show',
          'as'    => 'socials.show',
          'title' => 'socials.show',
        ]);
        # socials store
        Route::get('socials/create',
          [
            'uses'  => 'SocialController@create',
            'as'    => 'socials.create',
            'title' => 'socials.create_page',
          ]
        );

        # socials store
        Route::post('socials', [
          'uses'  => 'SocialController@store',
          'as'    => 'socials.store',
          'title' => 'socials.create',
        ]);
        # socials update
        Route::get('socials/{id}/edit', [
          'uses'  => 'SocialController@edit',
          'as'    => 'socials.edit',
          'title' => 'socials.edit_page',
        ]);
        # socials update
        Route::put('socials/{id}', [
          'uses'  => 'SocialController@update',
          'as'    => 'socials.update',
          'title' => 'socials.edit',
        ]);

        # socials delete
        Route::delete('socials/{id}', [
          'uses'  => 'SocialController@destroy',
          'as'    => 'socials.delete',
          'title' => 'socials.delete',
        ]);

        #delete all socials
        Route::post('delete-all-socials', [
          'uses'  => 'SocialController@destroyAll',
          'as'    => 'socials.deleteAll',
          'title' => 'socials.delete_all',
        ]);
      /*------------ end Of socials ----------*/

      /*------------ start Of seos ----------*/
        Route::get('seos', [
          'uses'  => 'SeoController@index',
          'as'    => 'seos.index',
          'title' => 'seo.index',
          'sub_link'  => true ,
        ]);
        # seos update
        Route::get('seos/{id}/Show', [
          'uses'  => 'SeoController@show',
          'as'    => 'seos.show',
          'title' => 'seo.show',
        ]);

        # seos store
        Route::get('seos/create', [
          'uses'  => 'SeoController@create',
          'as'    => 'seos.create',
          'title' => 'seo.create_page',
        ]);

        # seos update
        Route::get('seos/{id}/edit',
          [
            'uses'  => 'SeoController@edit',
            'as'    => 'seos.edit',
            'title' => 'seo.edit_page',
          ]
        );

        #store
        Route::post('seos/store',
          [
            'uses'  => 'SeoController@store',
            'as'    => 'seos.store',
            'title' => 'seo.create',
          ]
        );

        #update
        Route::put('seos/{id}',
          [
            'uses'  => 'SeoController@update',
            'as'    => 'seos.update',
            'title' => 'seo.edit',
          ]
        );

        #deletّe
        Route::delete('seos/{id}', [
          'uses'  => 'SeoController@destroy',
          'as'    => 'seos.delete',
          'title' => 'seo.delete',
        ]);
        #delete
        Route::post('delete-all-seos', [
          'uses'  => 'SeoController@destroyAll',
          'as'    => 'seos.deleteAll',
          'title' => 'seo.delete_all',
        ]);
      /*------------ end Of seos ----------*/

      /*------------ start Of Roles----------*/
        Route::get('roles', [
          'uses'  => 'RoleController@index',
          'as'    => 'roles.index',
          'title' => 'roles.index',
          'sub_link'  => true ,
        ]);

        #add role page
        Route::get('roles/create', [
          'uses'  => 'RoleController@create',
          'as'    => 'roles.create',
          'title' => 'roles.create_page',

        ]);

        #store role
        Route::post('roles/store', [
          'uses'  => 'RoleController@store',
          'as'    => 'roles.store',
          'title' => 'roles.create',
        ]);

        #edit role page
        Route::get('roles/{id}/edit',
          [
            'uses'  => 'RoleController@edit',
            'as'    => 'roles.edit',
            'title' => 'roles.edit_page',
          ]
        );

        #update role
        Route::put('roles/{id}', [
          'uses'  => 'RoleController@update',
          'as'    => 'roles.update',
          'title' => 'roles.edit',
        ]);

        #delete role
        Route::delete('roles/{id}', [
          'uses'  => 'RoleController@destroy',
          'as'    => 'roles.delete',
          'title' => 'roles.delete',
        ]);
      /*------------ end Of Roles----------*/

      /*------------ start Of reports----------*/
        Route::get('reports', [
          'uses'      => 'ReportController@index',
          'as'        => 'reports',
          'sub_link'  => true ,
          'title'     => 'reports.index',
        ]);

        # reports show
        Route::get('reports/{id}', [
          'uses'  => 'ReportController@show',
          'as'    => 'reports.show',
          'title' => 'reports.show',
        ]);
        # reports delete
        Route::delete('reports/{id}', [
          'uses'  => 'ReportController@destroy',
          'as'    => 'reports.delete',
          'title' => 'reports.delete',
        ]);

        #delete all reports
        Route::post('delete-all-reports', [
          'uses'  => 'ReportController@destroyAll',
          'as'    => 'reports.deleteAll',
          'title' => 'reports.delete_all',
        ]);
      /*------------ end Of reports ----------*/

      /*------------ start Of Settings----------*/
        Route::get('settings', [
          'uses'  => 'SettingController@index',
          'as'    => 'settings.index',
          'title' => 'setting.index',
          'sub_link'  => true ,
        ]);

        #update
        Route::put('settings', [
          'uses'  => 'SettingController@update',
          'as'    => 'settings.update',
          'title' => 'setting.edit',
        ]);

      /*------------ end Of Settings ----------*/

    /*------------ END Of public settings ----------*/

    /*------------ start Of public sections ----------*/
        Route::get('public-sections', [
          'as'        => 'public-sections',
          'icon'      => '<i class="feather icon-flag"></i>',
          'title'     => 'public_sections',
          'type'      => 'parent',
          'has_sub_route' => true,
          'child'     => [
              'intros.index', 'intros.show', 'intros.create', 'intros.store', 'intros.edit', 'intros.update', 'intros.delete', 'intros.deleteAll',
              'images.index', 'images.show', 'images.create', 'images.store', 'images.edit', 'images.update', 'images.delete', 'images.deleteAll',
              'all_complaints', 'complaints.delete', 'complaints.deleteAll', 'complaints.show', 'complaint.replay',
          ],
        ]);

      /*------------ start Of intros ----------*/
        Route::get('intros', [
          'uses'      => 'IntroController@index',
          'as'        => 'intros.index',
          'title'     => 'definition_pages.index',
          'sub_link'  => true ,
        ]);

        # intros update
        Route::get('intros/{id}/Show', [
          'uses'  => 'IntroController@show',
          'as'    => 'intros.show',
          'title' => 'definition_pages.show',
        ]);

        # intros store
        Route::get('intros/create',
          [
            'uses'  => 'IntroController@create',
            'as'    => 'intros.create',
            'title' => 'definition_pages.create_page',
          ]
        );

        # intros store
        Route::post('intros/store',
          [
            'uses'  => 'IntroController@store',
            'as'    => 'intros.store',
            'title' => 'definition_pages.create',
          ]
        );

        # intros update
        Route::get('intros/{id}/edit', [
          'uses'  => 'IntroController@edit',
          'as'    => 'intros.edit',
          'title' => 'definition_pages.edit_page',
        ]);

        # intros update
        Route::put('intros/{id}', [
          'uses'  => 'IntroController@update',
          'as'    => 'intros.update',
          'title' => 'definition_pages.edit',
        ]);

        # intros delete
        Route::delete('intros/{id}',
          [
            'uses'  => 'IntroController@destroy',
            'as'    => 'intros.delete',
            'title' => 'definition_pages.delete',
          ]
        );
        #delete all intros
        Route::post('delete-all-intros', [
          'uses'  => 'IntroController@destroyAll',
          'as'    => 'intros.deleteAll',
          'title' => 'definition_pages.delete_all',
        ]);
      /*------------ end Of intros ----------*/

      /*------------ start Of images ----------*/
        Route::get('images', [
          'uses'      => 'ImageController@index',
          'as'        => 'images.index',
          'title'     => 'advertising_banners.index',
          'sub_link'  => true ,
        ]);
        Route::get('images/{id}/show', [
          'uses'  => 'ImageController@show',
          'as'    => 'images.show',
          'title' => 'advertising_banners.show',
        ]);
        # images store
        Route::get('images/create', [
          'uses'  => 'ImageController@create',
          'as'    => 'images.create',
          'title' => 'advertising_banners.create_page',
        ]);

        # images store
        Route::post('images/store',
          [
            'uses'  => 'ImageController@store',
            'as'    => 'images.store',
            'title' => 'advertising_banners.create',
          ]
        );

        # images update
        Route::get('images/{id}/edit', [
          'uses'  => 'ImageController@edit',
          'as'    => 'images.edit',
          'title' => 'advertising_banners.edit_page',
        ]);

        # images update
        Route::put('images/{id}', [
          'uses'  => 'ImageController@update',
          'as'    => 'images.update',
          'title' => 'advertising_banners.edit',
        ]);

        # images delete
        Route::delete('images/{id}',
          [
            'uses'  => 'ImageController@destroy',
            'as'    => 'images.delete',
            'title' => 'advertising_banners.delete',
          ]
        );
        #delete all images
        Route::post('delete-all-images', [
          'uses'  => 'ImageController@destroyAll',
          'as'    => 'images.deleteAll',
          'title' => 'advertising_banners.delete_all',
        ]);
      /*------------ end Of images ----------*/

      /*------------ start Of complaints ----------*/
        Route::get('all-complaints', [
          'as'        => 'all_complaints',
          'uses'      => 'ComplaintController@index',
          'sub_link'  => true ,
          'title'     => 'complaints_and_proposals.index',
        ]);

        # complaint replay
        Route::post('complaints-replay/{id}', [
          'uses'  => 'ComplaintController@replay',
          'as'    => 'complaint.replay',
          'title' => 'complaints_and_proposals.the_replay_of_complaining_or_proposal',
        ]);
        # socials update
        Route::get('complaints/{id}', [
          'uses'  => 'ComplaintController@show',
          'as'    => 'complaints.show',
          'title' => 'complaints_and_proposals.the_resolution_of_complaining_or_proposal',
        ]);


        # complaints delete
        Route::delete('complaints/{id}', [
          'uses'  => 'ComplaintController@destroy',
          'as'    => 'complaints.delete',
          'title' => 'complaints_and_proposals.delete',
        ]);

        #delete all complaints
        Route::post('delete-all-complaints', [
          'uses'  => 'ComplaintController@destroyAll',
          'as'    => 'complaints.deleteAll',
          'title' => 'complaints_and_proposals.delete_all',
        ]);
      /*------------ end Of complaints ----------*/

    /*------------ end Of public sections ----------*/

    /*------------ start Of notifications ----------*/
      Route::get('notifications', [
        'uses'      => 'NotificationController@index',
        'as'        => 'notifications.index',
        'title'     => 'notifications.index',
        'icon'      => '<i class="ficon feather icon-bell"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['notifications.send'],
      ]);

      Route::post('send-notifications', [
        'uses'  => 'NotificationController@sendNotifications',
        'as'    => 'notifications.send',
        'title' => 'notifications.send',
      ]);
    /*------------ end Of notifications ----------*/

    /*------------ start Of categories ----------*/
      Route::get('categories-show/{id?}', [
        'uses'      => 'CategoryController@index',
        'as'        => 'categories.index',
        'title'     => 'sections.index',
        'icon'      => '<i class="feather icon-list"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['categories.create', 'categories.store', 'categories.edit', 'categories.update', 'categories.delete', 'categories.deleteAll', 'categories.show'],
      ]);

      # categories store
      Route::get('categories/create/{id?}', [
        'uses'  => 'CategoryController@create',
        'as'    => 'categories.create',
        'title' => 'sections.create_page',
      ]);

      # categories store
      Route::post('categories/store', [
        'uses'  => 'CategoryController@store',
        'as'    => 'categories.store',
        'title' => 'sections.create',
      ]);

      # categories update
      Route::get('categories/{id}/edit', [
        'uses'  => 'CategoryController@edit',
        'as'    => 'categories.edit',
        'title' => 'sections.edit_page',
      ]);

      # categories update
      Route::put('categories/{id}', [
        'uses'  => 'CategoryController@update',
        'as'    => 'categories.update',
        'title' => 'sections.edit',
      ]);

      Route::get('categories/{id}/show', [
        'uses'  => 'CategoryController@show',
        'as'    => 'categories.show',
        'title' => 'sections.show',
      ]);

      # categories delete
      Route::delete('categories/{id}', [
        'uses'  => 'CategoryController@destroy',
        'as'    => 'categories.delete',
        'title' => 'sections.delete',
      ]);
      #delete all categories
      Route::post('delete-all-categories', [
        'uses'  => 'CategoryController@destroyAll',
        'as'    => 'categories.deleteAll',
        'title' => 'sections.delete_all',
      ]);
    /*------------ end Of categories ----------*/

    /*------------ start Of coupons ----------*/
      Route::get('coupons', [
        'uses'      => 'CouponController@index',
        'as'        => 'coupons.index',
        'title'     => 'coupons.index',
        'icon'      => '<i class="fa fa-gift"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['coupons.show', 'coupons.create', 'coupons.store', 'coupons.edit', 'coupons.update', 'coupons.delete', 'coupons.deleteAll', 'coupons.renew'],
      ]);

      Route::get('coupons/{id}/show', [
        'uses'  => 'CouponController@show',
        'as'    => 'coupons.show',
        'title' => 'coupons.show',
      ]);

      # coupons store
      Route::get('coupons/create', [
        'uses'  => 'CouponController@create',
        'as'    => 'coupons.create',
        'title' => 'coupons.create_page',
      ]);

      # coupons store
      Route::post('coupons/store', [
        'uses'  => 'CouponController@store',
        'as'    => 'coupons.store',
        'title' => 'coupons.create',
      ]);

      # coupons update
      Route::get('coupons/{id}/edit', [
        'uses'  => 'CouponController@edit',
        'as'    => 'coupons.edit',
        'title' => 'coupons.edit_page',
      ]);

      # coupons update
      Route::put('coupons/{id}', [
        'uses'  => 'CouponController@update',
        'as'    => 'coupons.update',
        'title' => 'coupons.edit',
      ]);

      # renew coupon
      Route::post('coupons/renew', [
        'uses'  => 'CouponController@renew',
        'as'    => 'coupons.renew',
        'title' => 'coupons.update_coupon_status',
      ]);

      # coupons delete
      Route::delete('coupons/{id}', [
        'uses'  => 'CouponController@destroy',
        'as'    => 'coupons.delete',
        'title' => 'coupons.delete',
      ]);
      #delete all coupons
      Route::post('delete-all-coupons', [
        'uses'  => 'CouponController@destroyAll',
        'as'    => 'coupons.deleteAll',
        'title' => 'coupons.delete_all',
      ]);
    /*------------ end Of coupons ----------*/

    /*------------ start Of fqs ----------*/
      Route::get('fqs', [
        'uses'      => 'FqsController@index',
        'as'        => 'fqs.index',
        'title'     => 'questions_sections.index',
        'icon'      => '<i class="feather icon-alert-circle"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['fqs.show', 'fqs.create', 'fqs.store', 'fqs.edit', 'fqs.update', 'fqs.delete', 'fqs.deleteAll'],
      ]);

      Route::get('fqs/{id}/show', [
        'uses'  => 'FqsController@show',
        'as'    => 'fqs.show',
        'title' => 'questions_sections.show',
      ]);

      # fqs store
      Route::get('fqs/create', [
        'uses'  => 'FqsController@create',
        'as'    => 'fqs.create',
        'title' => 'questions_sections.create_page',
      ]);

      # fqs store
      Route::post('fqs/store', [
        'uses'  => 'FqsController@store',
        'as'    => 'fqs.store',
        'title' => 'questions_sections.create',
      ]);

      # fqs update
      Route::get('fqs/{id}/edit', [
        'uses'  => 'FqsController@edit',
        'as'    => 'fqs.edit',
        'title' => 'questions_sections.edit_page',
      ]);

      # fqs update
      Route::put('fqs/{id}', [
        'uses'  => 'FqsController@update',
        'as'    => 'fqs.update',
        'title' => 'questions_sections.edit',
      ]);

      # fqs delete
      Route::delete('fqs/{id}', [
        'uses'  => 'FqsController@destroy',
        'as'    => 'fqs.delete',
        'title' => 'questions_sections.delete',
      ]);
      #delete all fqs
      Route::post('delete-all-fqs', [
        'uses'  => 'FqsController@destroyAll',
        'as'    => 'fqs.deleteAll',
        'title' => 'questions_sections.delete_all',
      ]);
    /*------------ end Of fqs ----------*/

    /*------------ start Of sms ----------*/
      Route::get('sms', [
        'uses'      => 'SMSController@index',
        'as'        => 'sms.index',
        'title'     => 'message_packages.index',
        'icon'      => '<i class="feather icon-smartphone"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['sms.update', 'sms.change'],
      ]);
      # sms change
      Route::post('sms-change', [
        'uses'  => 'SMSController@change',
        'as'    => 'sms.change',
        'title' => 'message_packages.change',
      ]);
      # sms update
      Route::put('sms/{id}', [
        'uses'  => 'SMSController@update',
        'as'    => 'sms.update',
        'title' => 'message_packages.update',
      ]);

    /*------------ end Of sms ----------*/

    /*------------ start Of statistics ----------*/
      Route::get('statistics', [
        'uses'  => 'StatisticsController@index',
        'as'    => 'statistics.index',
        'title' => 'statistics',
        'icon'  => '<i class="feather icon-activity"></i>',
        'type'  => 'parent',
        'child' => [
          'statistics.index',
        ],
      ]);
    /*------------ end Of statistics ----------*/

    /*------------ start Of Settlements----------*/
      Route::get('settlements', [
        'uses'  => 'SettlementController@index',
        'as'    => 'settlements.index',
        'title' => 'settlement_requests',
        'icon'  => '<i class="feather icon-image"></i>',
        'type'  => 'parent',
        'has_sub_route' => false,
        'child' => [
          'settlements.show',
          'settlements.changeStatus',
        ],
      ]);

      #Show Settlement
      Route::get('settlements/show/{id}', [
        'uses'  => 'SettlementController@show',
        'as'    => 'settlements.show',
        'title' => 'view_settlement_order',
      ]);

      #Change Settlement Status
      Route::post('settlements/change-status', [
        'uses'  => 'SettlementController@settlementChangeStatus',
        'as'    => 'settlements.changeStatus',
        'title' => 'change_status_settlement_requests',
      ]);
    /*------------ end Of Settlements ----------*/

    /*------------ start Of pages ----------*/
        Route::get('pages', [
            'uses'      => 'pagesController@index',
            'as'        => 'pages.index',
            'title'     => 'pages.index',
            'icon'      => '<i class="feather icon-image"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['pages.edit', 'pages.update', 'pages.show']
        ]);


        # pages update
        Route::get('pages/{id}/edit', [
            'uses'  => 'pagesController@edit',
            'as'    => 'pages.edit',
            'title' => 'pages.edit_page'
        ]);

        # pages update
        Route::put('pages/{id}', [
            'uses'  => 'pagesController@update',
            'as'    => 'pages.update',
            'title' => 'pages.edit'
        ]);

        # pages show
        Route::get('pages/{id}/Show', [
            'uses'  => 'pagesController@show',
            'as'    => 'pages.show',
            'title' => 'pages.show',
        ]);
    /*------------ end Of pages ----------*/

    // /*------------ start Of paymentbrands ----------*/
    //     Route::get('paymentbrands', [
    //         'uses'      => 'paymentbrandController@index',
    //         'as'        => 'paymentbrands.index',
    //         'title'     => 'paymentbrands.index',
    //         'icon'      => '<i class="feather icon-image"></i>',
    //         'type'      => 'parent',
    //         'sub_route' => false,
    //         'child'     => ['paymentbrands.create', 'paymentbrands.store', 'paymentbrands.edit', 'paymentbrands.update', 'paymentbrands.show', 'paymentbrands.delete', 'paymentbrands.deleteAll']
    //     ]);

    //     # paymentbrands store
    //     Route::get('paymentbrands/create', [
    //         'uses'  => 'paymentbrandController@create',
    //         'as'    => 'paymentbrands.create',
    //         'title' => 'paymentbrands.create_page'
    //     ]);

    //     # paymentbrands store
    //     Route::post('paymentbrands/store', [
    //         'uses'  => 'paymentbrandController@store',
    //         'as'    => 'paymentbrands.store',
    //         'title' => 'paymentbrands.create'
    //     ]);

    //     # paymentbrands update
    //     Route::get('paymentbrands/{id}/edit', [
    //         'uses'  => 'paymentbrandController@edit',
    //         'as'    => 'paymentbrands.edit',
    //         'title' => 'paymentbrands.edit_page'
    //     ]);

    //     # paymentbrands update
    //     Route::put('paymentbrands/{id}', [
    //         'uses'  => 'paymentbrandController@update',
    //         'as'    => 'paymentbrands.update',
    //         'title' => 'paymentbrands.edit'
    //     ]);

    //     # paymentbrands show
    //     Route::get('paymentbrands/{id}/Show', [
    //         'uses'  => 'paymentbrandController@show',
    //         'as'    => 'paymentbrands.show',
    //         'title' => 'paymentbrands.show',
    //     ]);

    //     # paymentbrands delete
    //     Route::delete('paymentbrands/{id}', [
    //         'uses'  => 'paymentbrandController@destroy',
    //         'as'    => 'paymentbrands.delete',
    //         'title' => 'paymentbrands.delete',
    //     ]);

    //     # delete all paymentbrands
    //     Route::post('delete-all-paymentbrands', [
    //         'uses'  => 'paymentbrandController@destroyAll',
    //         'as'    => 'paymentbrands.deleteAll',
    //         'title' => 'paymentbrands.delete_all',
    //     ]);

    // /*------------ end Of paymentbrands ----------*/

    #new_routes_here



  });


  // redirect to home if url not found
  Route::fallback(function () {
    return redirect()->route('admin.show.login');
  });

  Route::get('export/{export}', 'ExcelController@master')->name('master-export');
  Route::post('import-items', 'ExcelController@importItems')->name('import-items');
});


