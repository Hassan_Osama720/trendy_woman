<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::create([
            'name' => ['en' => 'Basic', 'ar' => 'بيسك'],
            'description' => ['en' => 'Basic Package', 'ar' => 'بيسك'],
            'price' => 100,
            'is_active' => true,
        ]);

        Package::create([
            'name' => ['en' => 'Standard', 'ar' => 'ستاندرد'],
            'description' => ['en' => 'Standard Package', 'ar' => 'ستاندرد'],
            'price' => 200,
            'is_active' => true,
        ]);

        Package::create([
            'name' => ['en' => 'Premium', 'ar' => 'بريميوم'],
            'description' => ['en' => 'Premium Package', 'ar' => 'بريميوم'],
            'price' => 300,
            'is_active' => true,
        ]);

        Package::create([
            'name' => ['en' => 'Ultimate', 'ar' => 'ألتيميت'],
            'description' => ['en' => 'Ultimate Package', 'ar' => 'ألتيميت'],
            'price' => 400,
            'is_active' => true,
        ]);
    }
}
