<?php
namespace Database\Seeders;

use App\Models\IntroSlider;
use Illuminate\Database\Seeder;
use DB;

class IntroSliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('intro_sliders')->insert([
            [
                'image'       => '1.png' ,
            ] ,[
                'image'       => '2.png' ,
            ],[
                'image'       => '1.png' ,
            ],[
                'image'       => '2.png' ,
            ]
        ]);
       
    }
}
