<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BodyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('body_types')->insert([
            [
                'name' => json_encode([
                    'en' => 'Apple',
                    'ar' => 'تفاحة'
                ], JSON_UNESCAPED_UNICODE),
                'image' => 'default.png',
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode([
                    'en' => 'Triangle',
                    'ar' => 'مثلث'
                ], JSON_UNESCAPED_UNICODE),
                'image' => 'default.png',
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode([
                    'en' => 'Hourglass',
                    'ar' => 'ساعة الرمل'
                ], JSON_UNESCAPED_UNICODE),
                'image' => 'default.png',
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode([
                    'en' => 'Rectangle',
                    'ar' => 'مستطيل'
                ], JSON_UNESCAPED_UNICODE),
                'image' => 'default.png',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
