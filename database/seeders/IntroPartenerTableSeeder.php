<?php
namespace Database\Seeders;

use App\Models\IntroPartener;
use Illuminate\Database\Seeder;
use DB;

class IntroPartenerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('intro_parteners')->insert([
            [
                'image'   => '1.png'  ,
                'name'    => 'name 1' ,
            ] ,
            [
                'image'  => '3.png' ,
                'name'   => 'name 2' ,
            ] ,
            [
                'image'   => '4.png' ,
                'name'    => 'name 3' ,
            ]
        ]);
    }
}
