<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            [
                'name' => json_encode(['en' => 'Small', 'ar' => 'صغير'], JSON_UNESCAPED_UNICODE),
                'value' => 45,
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode(['en' => 'Medium', 'ar' => 'متوسط'], JSON_UNESCAPED_UNICODE),
                'value' => 50,
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode(['en' => 'Large', 'ar' => 'كبير'], JSON_UNESCAPED_UNICODE),
                'value' => 55,
                'created_at' => Carbon::now()
            ],[
                'name' => json_encode(['en' => 'Extra Large', 'ar' => 'كبير جدا'], JSON_UNESCAPED_UNICODE),
                'value' => 60,
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
