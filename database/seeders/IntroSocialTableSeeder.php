<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class IntroSocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('intro_socials')->insert([
            [
                'image'             => 'facebook.png',
                'key'              => 'facebook',
                'url'              => 'https://www.facebook.com',
            ],[
                'image'             => 'twitter.png',
                'key'              => 'twitter',
                'url'              => 'https://www.twitter.com',
            ],[
                'image'             => 'instagram.png',
                'key'              => 'instagram',
                'url'              => 'https://www.instagram.com',
            ]
        ]);
    }
}
