<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {
  public function up() {
    Schema::create('orders', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger('orderable_id')->nullable();
        $table->string('orderable_type')->nullable();
        $table->string('order_num', 50); //! create with new order dynamic
        $table->foreignId('user_id')->constrained()->onDelete('cascade');
        $table->double('price', 9, 2)->default(0);
        $table->double('balance', 9, 2)->default(0);
        $table->double('vat_per', 9, 2)->default(0);
        $table->double('vat_amount', 9, 2)->default(0);
        $table->double('final_total', 9, 2)->default(0);
        $table->string('status')->default(\App\Enums\OrderStatus::NEW); //! model const
        $table->string('name', 255)->nullable();
        $table->string('phone', 50)->nullable();
        $table->string('country_code', 255)->nullable();
        $table->integer('age');
        $table->enum('type', ['male', 'female']);
        $table->timestamp('date');
        $table->text('cancel_reason')->nullable();
        //$table->foreignId('chat_id')->constrained()->onDelete('cascade');
        $table->string('occasion', 255)->nullable();
        $table->timestamp('created_at')->useCurrent();
        $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
    });
  }

  public function down() {
    Schema::dropIfExists('orders');
  }
}
