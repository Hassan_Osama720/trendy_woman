<?php

return [
    'TestNotification'   => 'Test Notification Message En',
    'title_finish_order' => 'Awamer',
    'body_finish_order'  => 'Your order has been finished :order_num',
    'title_admin_notify' => 'Administrative notice',
    'title_block' => 'Block',
    'body_block'  => 'Your Account Is Block From Administrative',
    'new_order_title' => 'New Order',
    'new_order_body'  => 'New Order Added With Number: :order_num',
    'title_order_cancel' => 'Order Cancel',
    'body_order_cancel'  => 'Order Cancel With Number: :order_num',
    'title_order_in_progress' => 'Customer Paid Order Price',
    'body_order_in_progress'  => 'Customer Paid Order Price With Number: :order_num',
    'title_order_accepted' => 'Order Accepted',
    'body_order_accepted'  => 'Order Accepted With Number: :order_num',
    'title_order_finished' => 'Order Finished',
    'body_order_finished'  => 'Order Finished With Number: :order_num',
];
