<?php

return [

    'TestNotification'   => 'اختبار الاشعارات',
    'title_finish_order' => 'Awamer',
    'body_finish_order'  => 'طلبك تم :order_num',
    'title_admin_notify' => 'اشعار اداري ',
    'title_finish_order' => 'انهاء طلب',
    'body_finish_order'  => 'تم الانتهاء من المنتج رقم :order_num',
    'title_block' => 'حظر',
    'body_block'  => 'تم حظرك من قبل الادارة',
    'new_order_title' => 'طلب جديد',
    'new_order_body'  => 'تم اضافة طلب جديد رقم :order_num',
    'title_order_cancel' => 'الغاء الطلب',
    'body_order_cancel'  => 'تم الغاء الطلب رقم :order_num',
    'title_order_in_progress' => 'قام العميل بدفع سعر الطلب',
    'body_order_in_progress'  => 'قام العميل بدفع سعر الطلب رقم :order_num',
    'title_order_accepted' => 'تم قبول الطلب',
    'body_order_accepted'  => 'تم قبول الطلب رقم :order_num',
    'title_order_finished' => 'تم انهاء الطلب',
    'body_order_finished'  => 'تم انهاء الطلب رقم :order_num',

    
];
